# import matplotlib
# matplotlib.use('Gtk3Agg')
import matplotlib.pyplot as plt
import random


class PlotGenerator:

    forecast = []
    values = []

    def __init__(self, values, forecast):
        self.values = values
        self.forecast = forecast

    def show(self):
        x = list(range(1, len(self.values) + 1))
        plt.figure()
        plt.subplot(211)
        plt.ylabel('Medium value of owner occupied homes in $1000\'s')
        plt.xlabel('Samples')
        plt.plot(x, self.values, 'b', label="Real data")
        plt.plot(x, self.forecast, 'r', label="Trained model")
        plt.yticks(list(range(0, int(max(self.values)) + 10, 5)))
        plt.xticks(list(range(0, 101, 10)))
        plt.legend(bbox_to_anchor=(0.0, 1.02, 1.0, 0.102), loc=3, ncol=1, borderaxespad=0.0)

        plt.subplot(212)
        plt.ylabel('Medium value of owner occupied homes in $1000\'s')
        plt.xlabel('Samples')
        plt.plot(x, self.values, 'bo', label="Real data")
        plt.plot(x, self.forecast, 'r^', label="Trained model")
        plt.legend(bbox_to_anchor=(0.0, 1.02, 1.0, 0.102), loc=3, ncol=1, borderaxespad=0.0)
        plt.yticks(list(range(0, int(max(self.values)) + 10, 5)))
        plt.xticks(list(range(0, 101, 10)))
        plt.grid()
        plt.show()

if __name__ == "__main__":
    values = predictions = []
    x = list(range(1, 51))
    for i in range(50):
        v = random.uniform(5.0, 25.0)
        p = random.uniform(5.0, 25.0)
        values.append(v)
        predictions.append(p)

    plot = PlotGenerator(values, predictions)
    plot.show()
