import csv


def parse(filename):
    with open(filename, "r") as f_obj:
        return csv_dict_reader(f_obj)


def csv_dict_reader(file_obj):
    record_list = []
    reader = csv.DictReader(file_obj, delimiter=',')
    for line in reader:
        new_record = Record(float(line["CRIM"]), float(line["ZN"]), float(line["INDUS"]),
            float(line["CHAS"]), float(line["NOX"]), float(line["RM"]), float(line["AGE"]),
            float(line["DIS"]), float(line["RAD"]), float(line["TAX"]), float(line["PTRATIO"]),
            float(line["B"]), float(line["LSTAT"]), float(line["MEDV"]))
        if new_record.is_valid_record():
            record_list.append(new_record)
    return record_list


class Record:
    def __init__(self, crime_rate, zoned_land, non_retail, river_bound, nitric_oxide, number_rooms, old_units,
                 employement_distance, highway_access, property_tax, pupil_teacher, black_people, lower_status,
                 median_value=None):
        self.crime_rate = crime_rate
        self.zoned_land = zoned_land
        self.non_retail = non_retail
        self.river_bound = river_bound
        self.nitric_oxide = nitric_oxide
        self.number_rooms = number_rooms
        self.old_units = old_units
        self.employement_distance = employement_distance
        self.highway_access = highway_access
        self.property_tax = property_tax
        self.pupil_teacher = pupil_teacher
        self.black_people = black_people
        self.lower_status = lower_status
        self.median_value = median_value


    def is_valid_record(self):
        dictonary = self.__dict__
        for key in dictonary:
                if dictonary[key]=="":
                    return False
        return True

    def get_array_of_inputs(self):
        dictonary = self.__dict__
        keys = []
        for key in dictonary:
            keys.append(key)
        keys.remove("median_value")
        keys.sort()
        values = []
        for key in keys:
            values.append(dictonary[key])
        return values

    def get_output_value(self):
        return self.median_value

if __name__ == '__main__':
    parse("./dataset.csv")



