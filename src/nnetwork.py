from logging import getLogger

import numpy
from sklearn.preprocessing import MinMaxScaler
from sknn.mlp import Regressor, Layer


class NNetworkException(Exception):
    pass


class NeuralNetwork:
    def __init__(self, learning_rate=0.01, n_iterations=None):
        self._logger = getLogger(__name__)
        self._trained = False
        self._network = Regressor(layers=[
            Layer('Sigmoid', units=1),
            Layer('Sigmoid', units=59),
            Layer('Linear')
            ],
            learning_rate=learning_rate,
            random_state=0,
        )
        if n_iterations:
            self._network.n_iter = n_iterations
            self._network.n_stable = n_iterations
        self._data_scaler = MinMaxScaler()
        self._target_scaler = MinMaxScaler()

    def train(self, dataset):
        self._logger.info('Starting to train neural network.')
        input_data = []
        expected_output = []
        for record in dataset:
            input_data.append(record.get_array_of_inputs())
            expected_output.append(record.get_output_value())
        input_data = self._to_numpy_array(input_data, rows=len(input_data), columns=13)
        expected_output = self._to_numpy_array(expected_output, rows=len(expected_output), columns=1)
        self._logger.debug('Received training inputs: ' + str(input_data) + '\nShape: ' + str(input_data.shape))
        self._logger.debug('Received expected outputs: ' + str(expected_output) + '\nShape: ' +
                           str(expected_output.shape))
        input_data = self._data_scaler.fit_transform(input_data)
        expected_output = self._target_scaler.fit_transform(expected_output)
        self._logger.debug('Scaled training inputs: ' + str(input_data) + '\nShape: ' + str(input_data.shape))
        self._logger.debug('Scaled expected outputs: ' + str(expected_output) + '\nShape: ' +
                           str(expected_output.shape))
        try:
            self._network.fit(input_data, expected_output)
            self._trained = True
        except Exception as e:
            self._logger.error('The neural network raised exception during training. Cause: ' + str(e))
            raise NNetworkException(e)
        self._logger.info('Finished training neural network.')

    def forecast(self, record):
        if not self._trained:
            self._logger.warning('A forecast was attempted on an uninitialized network.')
            raise NNetworkException('Cannot forecast values before the network is properly trained.')
        try:
            array = self._to_numpy_array(record.get_array_of_inputs(), rows=1, columns=13)
            prediction = self._network.predict(self._data_scaler.transform(array))
            return self._target_scaler.inverse_transform(prediction)[0][0]
        except Exception as e:
            self._logger.warning('An exception was raised during forecast operation. Cause: ' + str(e) + ', Record: ' +
                                 str(record))
            raise ValueError('A valid Record object is required for the forecast operation.')

    def _to_numpy_array(self, buffer_list, rows, columns):
        return numpy.ndarray(shape=(rows, columns), buffer=numpy.array(buffer_list))


def mean_square_error(network, set):
    sum_of_square_errors = 0
    n_elements = 0
    for record in set:
        prediction = network.forecast(record)
        sum_of_square_errors += (record.get_output_value() - prediction) ** 2
        n_elements += 1
    return sum_of_square_errors / n_elements
