import argparse
import logging
import os

from terminaltables import AsciiTable

from src import nnetwork, parser, plotGenerator


class Style:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def is_file(x):
    if not os.path.exists(x):
        raise argparse.ArgumentTypeError("{0} does not exist".format(x))
    elif not x.endswith(".csv"):
        raise argparse.ArgumentTypeError("{0} is not a CSV file".format(x))
    return x


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-v",
        "--verbose",
        help="Enable verbose mode",
        action='store_true'
    )
    parser.add_argument(
        "-csv",
        help="CSV file containing the dataset",
        type=is_file,
        default="src/dataset.csv"
    )
    parser.add_argument(
        "-iter",
        help="Number of iterations. Default is None",
        type=int,
        default=None
    )
    parser.add_argument(
        "-lr",
        help="Learning rate. Default is 0.01",
        type=float,
        default=0.01
    )

    return parser.parse_args()


def run():
    logging.basicConfig(level=logging.INFO)
    args = parse_args()

    if hasattr(args, "v") or hasattr(args, "verbose"):
        logging.basicConfig(level=logging.DEBUG)

    table_data = [['Value', 'Forecast', "Error"]]
    dataset = parser.parse(args.csv)
    train_data = dataset[:-100]
    test_data = dataset[-100:]

    n = nnetwork.NeuralNetwork(args.lr, args.iter)
    n.train(train_data)

    forecast = []
    values = []
    for record in test_data:
        prediction = n.forecast(record).round(1)
        forecast.append(prediction)
        values.append(record.get_output_value())
        row = [str(record.get_output_value()), str(prediction),
               str(round((record.get_output_value() - float(prediction)), 1))]
        table_data.append(row)

    table = AsciiTable(table_data)
    print(table.table)
    print(Style.OKBLUE + Style.BOLD + "\nMean Square Error : " +
          str(nnetwork.mean_square_error(n, test_data)) + Style.ENDC + Style.ENDC)
    plot = plotGenerator.PlotGenerator(values, forecast)
    plot.show()

if __name__ == '__main__':
    try:
        run()
    except Exception as e:
        print(Style.FAIL + str(e) + Style.ENDC)
